<?php
namespace Backtheweb\Linguo\Engines;

use Backtheweb\Linguo\Contracts\CatalogInterface;
use Backtheweb\Linguo\Contracts\TranslationInterface;

/**
 * Class to load a PO file.
 */
class Po extends EngineAbstract
{
    /**
     * @param CatalogInterface $catalog
     * @param string $filename
     * @return bool
     */
    public function generateFile(CatalogInterface $catalog, string $filename): bool
    {
        $content = $this->generateString($catalog);

        return file_put_contents($filename, $content) !== false;
    }

    /**
     * @param string $string
     * @param CatalogInterface|null $catalog
     * @return CatalogInterface
     */
    public function loadString( string $string, CatalogInterface $catalog = null ): CatalogInterface
    {
        $catalog     = parent::loadString($string, $catalog);
        $lines       = explode("\n", $string);
        $line        = current($lines);
        $translation = $this->createTranslation(null, '');

        while ( $line !== false ) {

            $line     = trim($line);
            $nextLine = next($lines);

            //Multiline
            while ( substr($line, -1, 1) === '"' && $nextLine !== false  && substr(trim($nextLine), 0, 1) === '"' ) {

                $line     = substr($line, 0, -1).substr(trim($nextLine), 1);
                $nextLine = next($lines);
            }

            //End of translation
            if ($line === '') {

                if ( !$translation->isEmpty() ) {

                    $catalog->add($translation);
                }

                $translation = $this->createTranslation(null, '');
                $line        = $nextLine;

                continue;
            }

            $splitLine = preg_split('/\s+/', $line, 2);
            $key       = $splitLine[0];
            $data      = $splitLine[1] ?? '';

            if ($key === '#~') {

                $translation->disable();

                $splitLine = preg_split('/\s+/', $data, 2);
                $key       = $splitLine[0];
                $data      = $splitLine[1] ?? '';
            }

            if ($data === '') {

                $line = $nextLine;
                continue;
            }

            switch ($key) {

                case '#':            $translation->getComments()        ->add($data); break;
                //case '#.':         $translation->extractedComments()  ->add($data);  break;
                case 'msgctxt':      $translation = $translation->withContext(  self::decode($data) );  break;
                case 'msgid':        $translation = $translation->withOriginal( self::decode($data) );  break;
                case 'msgid_plural': $translation->setPlural(                   self::decode($data) );  break;
                case 'msgstr':
                case 'msgstr[0]':    $translation->translate(                   self::decode($data) );  break;
                case 'msgstr[1]':    $translation->translatePlural(             self::decode($data) );  break;

                case '#,':

                    foreach (array_map('trim', explode(',', trim($data))) as $value) {

                        $translation->getFlags()->add($value);
                    }

                    break;

                case '#:':

                    foreach (preg_split('/\s+/', trim($data)) as $value) {

                        if (preg_match('/^(.+)(:(\d*))?$/U', $value, $matches)) {

                            $line = isset($matches[3]) ? intval($matches[3]) : null;
                            $translation->getReferences()->add($matches[1], $line);
                        }
                    }

                    break;

                default:

                    if (strpos($key, 'msgstr[') === 0) {

                        $p   = $translation->getPluralTranslations();
                        $p[] = self::decode($data);

                        $translation->translatePlural(...$p);

                        break;
                    }

                    break;
            }

            $line = $nextLine;
        }

        if ( !$translation->isEmpty() ) {

            $catalog->add($translation);
        }

        //Headers
        $translation = $catalog->find(null, '');

        if ( $translation ) {

            $catalog->remove($translation);

            $headers = $catalog->getHeaders();

            foreach ( self::parseHeaders( $translation->getTranslation() ) as $name => $value) {

                $headers->set($name, $value);
            }
        }

        return $catalog;
    }

    /**
     * @param string|null $string
     * @return array
     */
    private static function parseHeaders( ?string $string ): array
    {
        if (empty($string)) {
            return [];
        }

        $headers = [];
        $lines   = explode("\n", $string);
        $name    = null;

        foreach ($lines as $line) {

            $line = self::decode($line);

            if ($line === '') {
                continue;
            }

            // Checks if it is a header definition line.
            // Useful for distinguishing between header definitions and possible continuations of a header entry.

            if (preg_match('/^[\w-]+:/', $line)) {

                $pieces             = array_map('trim', explode(':', $line, 2));
                list($name, $value) = $pieces;
                $headers[$name]     = $value;

                continue;
            }

            $value          = $headers[$name] ?? '';
            $headers[$name] = $value.$line;
        }

        return $headers;
    }

    /**
     * @param CatalogInterface $catalog
     * @return string
     */
    public function generateString( CatalogInterface $catalog ): string
    {
        $pluralForm = $catalog->getHeaders()->getPluralForm();
        $pluralSize = is_array($pluralForm) ? ($pluralForm[0] - 1) : null;

        //Headers
        $lines = [
            'msgid ""',
            'msgstr ""'
        ];

        foreach ( $catalog->getHeaders() as $name => $value ) {

            $lines[] = sprintf('"%s: %s\\n"', $name, $value);
        }

        $lines[] = '';

        /** @var TranslationInterface $translation */
        foreach ($catalog as $translation) {

            foreach ($translation->getComments() as $comment) {

                $lines[] = sprintf('# %s', $comment);
            }

            foreach ($translation->getExtractedComments() as $comment) {

                $lines[] = sprintf('#. %s', $comment);
            }

            foreach ($translation->getReferences() as $filename => $lineNumbers) {

                if (empty($lineNumbers)) {
                    $lines[] = sprintf('#: %s', $filename);
                    continue;
                }

                foreach ($lineNumbers as $number) {
                    $lines[] = sprintf('#: %s:%d', $filename, $number);
                }
            }

            if (count($translation->getFlags())) {

                $lines[] = sprintf('#, %s', implode(',', $translation->getFlags()->toArray()));
            }

            $prefix = $translation->isDisabled() ? '#~ ' : '';

            if ($context = $translation->getContext()) {

                $lines[] = sprintf('%smsgctxt %s', $prefix, self::encode($context));
            }

            self::appendLines($lines, $prefix, 'msgid', $translation->getOriginal());

            if ($plural = $translation->getPlural()) {

                self::appendLines($lines, $prefix, 'msgid_plural', $plural);
                self::appendLines($lines, $prefix, 'msgstr[0]', $translation->getTranslation() ?: '');

                foreach ($translation->getPluralTranslations($pluralSize) as $k => $v) {
                    self::appendLines($lines, $prefix, sprintf('msgstr[%d]', $k + 1), $v);
                }

            } else {

                self::appendLines($lines, $prefix, 'msgstr', $translation->getTranslation() ?: '');
            }

            $lines[] = '';
        }

        return implode("\n", $lines);
    }

    /**
     * Add one or more lines depending whether the string is multiline or not.
     *
     * @param array $lines
     * @param string $prefix
     * @param string $name
     * @param string $value
     */
    private static function appendLines(array &$lines, string $prefix, string $name, string $value): void
    {
        $newLines = explode("\n", $value);
        $total    = count($newLines);

        if ($total === 1) {
            $lines[] = sprintf('%s%s %s', $prefix, $name, self::encode($newLines[0]));

            return;
        }

        $lines[] = sprintf('%s%s ""', $prefix, $name);
        $last    = $total - 1;

        foreach ($newLines as $k => $line) {

            if ($k < $last) {
                $line .= "\n";
            }

            $lines[] = self::encode($line);
        }
    }

    /**
     * Convert a string from its PO representation.
     *
     * @param string $value
     * @return string
     */
    public static function decode(string $value): string
    {
        if (!$value) {
            return '';
        }

        if ($value[0] === '"') {
            $value = substr($value, 1, -1);
        }

        return strtr( $value, [

            '\\\\' => '\\',
            '\\a'  => "\x07",
            '\\b'  => "\x08",
            '\\t'  => "\t",
            '\\n'  => "\n",
            '\\v'  => "\x0b",
            '\\f'  => "\x0c",
            '\\r'  => "\r",
            '\\"'  => '"',
        ]);
    }

    /**
     * Convert a string to its PO representation.
     *
     * @param string $value
     * @return string
     */
    public static function encode(string $value): string
    {
        return '"' . strtr( $value, [

            "\x00" => '',
            '\\'   => '\\\\',
            "\t"   => '\t',
            "\r"   => '\r',
            "\n"   => '\n',
            '"'    => '\\"',

        ] ).'"';
    }
}
