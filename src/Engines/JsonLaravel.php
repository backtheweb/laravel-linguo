<?php
namespace Backtheweb\Linguo\Engines;

use Backtheweb\Linguo\Contracts\CatalogInterface;
use Backtheweb\Linguo\Translator\Translation;

/**
 * Class JsonLaravel
 *
 * Create translations file in JSON format specific for Laravel
 *
 * @package Backtheweb\Linguo\Engines
 */
class JsonLaravel extends Json
{
    public function generateString(CatalogInterface $catalog): string
    {
        $array   = [];
        $catalog = $catalog->getTranslations();

        /** @var Translation $translation */
        foreach ($catalog as $translation) {
            $key         = $translation->getOriginal();
            $array[$key] = $translation->getTranslation();
            $plural      = $translation->getPlural();

            if($plural){
                $array[$plural] = $translation->getPluralTranslations(1)[0] ?? null;
            }
        }

        return json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
}
