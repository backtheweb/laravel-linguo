<?php
namespace Backtheweb\Linguo\Engines;

use Backtheweb\Linguo\Contracts\CatalogInterface;
use Backtheweb\Linguo\Contracts\TranslationInterface;
use Backtheweb\Linguo\Translator\Catalog;
use Backtheweb\Linguo\Translator\Headers;
use Backtheweb\Linguo\Translator\Translation;

/**
 * Class Json
 *
 * Create translations file in JSON format
 *
 * @package Backtheweb\Linguo\Engines
 */
class PhpArray extends EngineAbstract
{

    /**
     * @param CatalogInterface $catalog
     * @return string
     */
    public function generateString(CatalogInterface $catalog): string
    {

        $array = array_map(

            function (Catalog $catalog) {

                return [
                    $catalog->getOriginal() => $catalog->getTranslation(),
                ];
            },

            array_values( $catalog->getTranslations() )
        );

        return json_encode($array, $this->jsonOptions);
    }

    public function generateFile(CatalogInterface $catalog, string $filename): bool
    {
        $content = "\n";

        /** @var TranslationInterface $translation */
        foreach( $catalog->getTranslations() as $translation ){

            $key   = $translation->getOriginal();
            $value = $translation->getTranslation();

            $content .= sprintf("\t'%s' => '%s',\n", addslashes($key), addslashes($value) );
        }

        $content = sprintf("<?php return [%s];", $content);

        return file_put_contents($filename, $content) !== false;
    }

    public function generateArray(Catalog $catalog): array
    {
        $pluralForm = $catalog->getHeaders()->getPluralForm();
        $pluralSize = is_array($pluralForm) ? ($pluralForm[0] - 1) : null;
        $messages   = [];

        /** @var TranslationInterface $translation */
        foreach ($catalog as $translation) {

            if (!$translation->getTranslation() || $translation->isDisabled()) {
                continue;
            }

            $context  = $translation->getContext() ?: '';
            $original = $translation->getOriginal();

            if (!isset($messages[$context])) {

                $messages[$context] = [];
            }

            if ( $translation->hasPluralTranslations() ) {

                $messages[$context][$original] = $translation->getPluralTranslations($pluralSize);
                array_unshift($messages[$context][$original], $translation->getTranslation());

            } else {

                $messages[$context][$original] = $translation->getTranslation();
            }
        }

        return [
            'domain'       => $catalog->getDomain(),
            'plural-forms' => $catalog->getHeaders()->get(Headers::HEADER_PLURAL_FORM),
            'messages'     => $messages,
        ];
    }

    public function loadString(string $string, CatalogInterface $catalog = null): CatalogInterface
    {
        $array = json_decode($string, true);

        return $this->loadArray($array, $catalog);
    }

    public function loadArray(array $array, CatalogInterface $catalog = null): CatalogInterface
    {
        if (!$catalog) {
            $catalog = $this->createTranslations();
        }

        $messages = $array['messages'] ?? [];

        foreach ($messages as $context => $contextTranslations) {

            if ($context === '') {
                $context = null;
            }

            foreach ($contextTranslations as $original => $value) {

                if ($original === '') {
                    continue;
                }

                $catalog = $this->createTranslation($context, $original);
                $catalog->add($catalog);

                if (is_array($value)) {

                    $catalog->translate(array_shift($value));
                    $catalog->translatePlural(...$value);

                } else {

                    $catalog->translate($value);
                }
            }
        }

        if (!empty($array['domain'])) {

            $catalog->setDomain($array['domain']);
        }

        if (!empty($array['plural-forms'])) {

            $catalog->getHeaders()->set( Headers::HEADER_PLURAL_FORM, $array['plural-forms']);
        }

        return $catalog;
    }
}
