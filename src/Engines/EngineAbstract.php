<?php

namespace Backtheweb\Linguo\Engines;

use Backtheweb\Linguo\Contracts\GeneratorInterface;
use Backtheweb\Linguo\Contracts\CatalogInterface;
use Backtheweb\Linguo\Contracts\LoaderInterface;
use Backtheweb\Linguo\Contracts\TranslationInterface;
use Backtheweb\Linguo\Translator\Catalog;
use Backtheweb\Linguo\Translator\Translation;

use Exception;

abstract class EngineAbstract implements GeneratorInterface, LoaderInterface
{
    /**
     * @param string $string
     * @param CatalogInterface|null $catalog
     * @return CatalogInterface
     */
    public function loadString( string $string, CatalogInterface $catalog = null ): CatalogInterface
    {
        return $catalog ?: $this->createTranslations();
    }

    /**
     * @param CatalogInterface $catalog
     * @return string
     */
    abstract public function generateString(CatalogInterface $catalog): string;
    abstract public function generateFile(CatalogInterface $catalog, string $filename): bool;

    /*

        public function generateFile(CatalogInterface $catalog, string $filename): bool
    {
        $content = $this->generateString($catalog);

        return file_put_contents($filename, $content) !== false;
    }

     */

    /**
     * @return CatalogInterface
     */
    protected function createTranslations(): CatalogInterface
    {
        return Catalog::factory();
    }

    /**
     * @param string|null $context
     * @param string $original
     * @param string|null $plural
     * @return TranslationInterface
     */
    protected function createTranslation(?string $context, string $original, string $plural = null): TranslationInterface
    {
        return Translation::factory($original, $plural, null, null, $context);
    }

    /**
     * @param string $file
     * @return string
     * @throws Exception
     */
    protected static function readFile( string $file ): string
    {
        $length = filesize($file);

        if ( !($fd = fopen($file, 'rb')) ) {

            throw new Exception("Cannot read the file '$file', probably permissions");
        }

        $content = $length ? fread($fd, $length) : '';

        fclose($fd);

        return $content;
    }

    /**
     * @param string $filename
     * @param CatalogInterface|null $catalog
     * @return CatalogInterface
     * @throws Exception
     */
    public function loadFile( string $filename, CatalogInterface $catalog = null ): CatalogInterface
    {
        $string = static::readFile($filename);

        return $this->loadString($string, $catalog);
    }

    //abstract protected function createTranslations(): CatalogInterface;
    //abstract protected function createTranslation( ?string $context, string $original, string $plural = null ): TranslationInterface;
}
