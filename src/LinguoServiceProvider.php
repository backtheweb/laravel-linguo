<?php

namespace Backtheweb\Linguo;


use Illuminate\Support\ServiceProvider;

class LinguoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__.'/../config/linguo.php' => config_path('linguo.php')], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            Command\LinguoCommand::class,
            Command\CompileCommand::class,
            Command\ConvertCommand::class,
            Command\ScanCommand::class,
            Command\UpdateCommand::class,
        ]);
    }
}
