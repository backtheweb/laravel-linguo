<?php

namespace Backtheweb\Linguo\Translator;

use Backtheweb\Linguo\Contracts\CatalogInterface;

use ArrayIterator;
use Backtheweb\Linguo\Contracts\HeadersInterface;
use Backtheweb\Linguo\Contracts\TranslationInterface;
use Backtheweb\Linguo\Engines\Po;
use Countable;
use IteratorAggregate;

use InvalidArgumentException;


class Catalog implements CatalogInterface, Countable, IteratorAggregate
{
    const TRANSLATIONS_OURS         = 1 << 0;
    const TRANSLATIONS_THEIRS       = 1 << 1;
    const TRANSLATIONS_OVERRIDE     = 1 << 2;

    const HEADERS_OURS              = 1 << 3;
    const HEADERS_THEIRS            = 1 << 4;
    const HEADERS_OVERRIDE          = 1 << 5;

    const COMMENTS_OURS             = 1 << 6;
    const COMMENTS_THEIRS           = 1 << 7;

    const EXTRACTED_COMMENTS_OURS   = 1 << 8;
    const EXTRACTED_COMMENTS_THEIRS = 1 << 9;

    const FLAGS_OURS                = 1 << 10;
    const FLAGS_THEIRS              = 1 << 11;

    const REFERENCES_OURS           = 1 << 12;
    const REFERENCES_THEIRS         = 1 << 13;

    //Merge strategies
    const SCAN_AND_LOAD =
         Catalog::HEADERS_OVERRIDE |
         Catalog::TRANSLATIONS_OURS |
         Catalog::TRANSLATIONS_OVERRIDE |
         Catalog::EXTRACTED_COMMENTS_OURS |
         Catalog::REFERENCES_OURS |
         Catalog::FLAGS_THEIRS |
         Catalog::COMMENTS_THEIRS;

    /** @var array  */
    protected $translations = [];

    /** @var HeadersInterface */
    protected $headers;

    public static function factory(string $domain = null, string $locale = null): Catalog
    {
        $catalog = new static();

        if (isset($domain)) {
            $catalog->setDomain($domain);
        }

        if (isset($locale)) {
            $catalog->setLocale($locale);
        }

        return $catalog;
    }

    public function __construct()
    {
        $this->headers = new Headers();
    }

    public function __clone()
    {
        foreach ($this->translations as $id => $translation) {

            $this->translations[$id] = clone $translation;
        }

        $this->headers = clone $this->headers;
    }

    public function setDomain(string $domain): self
    {
        $this->getHeaders()->setDomain($domain);

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->getHeaders()->getDomain();
    }

    public function setLocale(string $locale): self
    {
        $this->getHeaders()
            ->setLocale($locale);

        /** @todo set plural form */

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->getHeaders()->getLocale();
    }

    public function toArray(): array
    {
        return [
            'headers'      => $this->headers->toArray(),
            'translations' => array_map(
                function ($translation) {
                    return $translation->toArray();
                },
                array_values($this->translations)
            ),
        ];
    }

    public function getIterator()
    {
        return new ArrayIterator($this->translations);
    }

    public function count(): int
    {
        return count($this->getTranslations());
    }

    public function getTranslations(): array
    {
        return $this->translations;
    }

    public function getHeaders(): HeadersInterface
    {
        return $this->headers;
    }

    public function add(Translation $translation): self
    {
        $this->translations[$translation->getId()] = $translation;

        return $this;
    }

    public function addOrMerge(Translation $translation, int $mergeStrategy = 0): self
    {
        $id = $translation->getId();

        if (isset($this->translations[$id])) {

            $this->translations[$id] = $this->translations[$id]->mergeWith($translation, $mergeStrategy);

        } else {

            $this->add($translation);
        }

        return $this;
    }

    public function remove(Translation $translation): self
    {
        $key = array_search($translation, $this->translations);

        if ($key !== false) {
            unset($this->translations[$key]);
        }

        return $this;
    }

    public function find(?string $context, string $original): ?TranslationInterface
    {
        foreach ($this->translations as $translation) {

            if ($translation->getContext() === $context && $translation->getOriginal() === $original) {
                return $translation;
            }
        }

        return null;
    }

    public function mergeWith(Catalog $catalog, int $strategy = 0): Catalog
    {
        $merged = clone $this;

        if ($strategy & static::HEADERS_THEIRS) {

            $merged->headers = clone $catalog->headers;

        } elseif (!($strategy & Catalog::HEADERS_OURS)) {

            $merged->headers = $merged->headers->mergeWith( $catalog->headers );
        }

        foreach ($catalog as $id => $translation) {

            if (isset($merged->translations[$id])) {

                $translation = $merged->translations[$id]->mergeWith($translation, $strategy);
            }

            $merged->add($translation);
        }

        if ($strategy & static::TRANSLATIONS_THEIRS) {

            $merged->translations = array_intersect_key($merged->translations, $catalog->translations);

        } elseif ($strategy & Catalog::TRANSLATIONS_OURS) {

            $merged->translations = array_intersect_key($merged->translations, $this->translations);
        }

        return $merged;
    }

    /*
    public function savePo($file) : bool
    {
        return ( new Po() )->generateFile($this, $file);
    }

    public static function readPo($file) : CatalogInterface
    {
        return ( new Po() )->loadFile($file);
    }
    */
}
