<?php

namespace Backtheweb\Linguo\Translator;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;

use Backtheweb\Linguo\Contracts\CommentsInterface;

class Comments implements CommentsInterface, JsonSerializable, Countable, IteratorAggregate
{
    protected $comments = [];

    public static function __set_state(array $state): Comments
    {
        return new static(...$state['comments']);
    }

    public function __construct(string ...$comments)
    {
        if (!empty($comments)) {
            $this->add(...$comments);
        }
    }

    public function getIterator()
    {
        return new ArrayIterator($this->comments);
    }

    public function count()
    {
        return count($this->comments);
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return $this->comments;
    }

    public function add(string ...$comments): CommentsInterface
    {
        foreach ($comments as $comment) {
            if (!in_array($comment, $this->comments)) {
                $this->comments[] = $comment;
            }
        }

        return $this;
    }

    public function mergeWith(Comments $comments): self
    {
        $merged = clone $this;
        $merged->add(...$comments->comments);

        return $merged;
    }
}
