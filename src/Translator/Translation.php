<?php

namespace Backtheweb\Linguo\Translator;

use Backtheweb\Linguo\Contracts\CommentsInterface;
use Backtheweb\Linguo\Contracts\FlagsInterface;
use Backtheweb\Linguo\Contracts\ReferencesInterface;
use Backtheweb\Linguo\Contracts\TranslationInterface;

class Translation implements TranslationInterface
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $context;

    /** @var string */
    protected $original;

    /** @var string */
    protected $plural;

    /** @var string */
    protected $translation;

    /** @var array  */
    protected $pluralTranslations = [];

    /** @var bool  */
    protected $disabled = false;

    /** @var ReferencesInterface  */
    protected $references;

    /** @var FlagsInterface  */
    protected $flags;

    /** @var CommentsInterface  */
    protected $comments;

    /** @var CommentsInterface  */
    protected $extractedComments;

    public static function factory( string $original, string $plural = null, string $file = null, $line = null, string $context = null ) : TranslationInterface
    {
        $id                    = static::generateId($context, $original);
        $translation           = new static($id);
        $translation->original = $original;
        $translation->plural   = $plural;
        $translation->context  = $context;

        if($file){

            $translation->references->add($file, $line);
        }

        return $translation;
    }

    protected static function generateId(?string $context, string $original): string
    {
        return "{$context}\004{$original}";
    }

    protected function __construct(string $id)
    {
        $this->id                   = $id;
        $this->references           = new References();
        $this->flags                = new Flags();
        $this->comments             = new Comments();
        $this->extractedComments    = new Comments();
    }

    public function __clone()
    {
        $this->references          = clone $this->references;
        $this->flags               = clone $this->flags;
        $this->comments            = clone $this->comments;
        $this->extractedComments   = clone $this->extractedComments;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function getOriginal(): string
    {
        return $this->original;
    }

    public function setPlural(string $plural): self
    {
        $this->plural = $plural;

        return $this;
    }

    public function getPlural(): ?string
    {
        return $this->plural;
    }

    public function disable(bool $disabled = true): TranslationInterface
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function translate(string $translation): TranslationInterface
    {
        $this->translation = $translation;

        return $this;
    }

    public function getPluralTranslations(int $size = null): array
    {
        if ($size === null) {
            return $this->pluralTranslations;
        }

        $length = count($this->pluralTranslations);

        if ($size > $length) {
            return $this->pluralTranslations + array_fill(0, $size, '');
        }

        return array_slice($this->pluralTranslations, 0, $size);
    }

    public function hasPluralTranslations(): bool
    {
        return implode('', $this->getPluralTranslations()) !== '';
    }

    public function translatePlural(string ...$translations): self
    {
        $this->pluralTranslations = $translations;

        return $this;
    }

    public function getTranslation(): ?string
    {
        return $this->translation;
    }

    public function toArray(): array
    {
        return [
            'id'                 => $this->id,
            'context'            => $this->context,
            'original'           => $this->original,
            'translation'        => $this->translation,
            'plural'             => $this->plural,
            'pluralTranslations' => $this->pluralTranslations,
            'disabled'           => $this->disabled,
            'references'         => $this->getReferences()        ->toArray(),
            'flags'              => $this->getFlags()             ->toArray(),
            'comments'           => $this->getComments()          ->toArray(),
            'extractedComments'  => $this->getExtractedComments() ->toArray(),
        ];
    }

    public function getReferences() : ReferencesInterface
    {
        return $this->references;
    }

    public function getFlags() : FlagsInterface
    {
        return $this->flags;
    }

    public function getComments() : CommentsInterface
    {
        return $this->comments;
    }

    public function getExtractedComments() : CommentsInterface
    {
        return $this->extractedComments;
    }

    public function mergeWith(Translation $translation, int $strategy = 0): Translation
    {
        $merged = clone $this;

        if ($strategy & Catalog::COMMENTS_THEIRS) {
            $merged->comments = clone $translation->comments;
        } elseif (!($strategy & Catalog::COMMENTS_OURS)) {
            $merged->comments = $merged->comments->mergeWith($translation->comments);
        }

        if ($strategy & Catalog::EXTRACTED_COMMENTS_THEIRS) {
            $merged->extractedComments = clone $translation->extractedComments;
        } elseif (!($strategy & Catalog::EXTRACTED_COMMENTS_OURS)) {
            $merged->extractedComments = $merged->extractedComments->mergeWith($translation->extractedComments);
        }

        if ($strategy & Catalog::REFERENCES_THEIRS) {
            $merged->references = clone $translation->references;
        } elseif (!($strategy & Catalog::REFERENCES_OURS)) {
            $merged->references = $merged->references->mergeWith($translation->references);
        }

        if ($strategy & Catalog::FLAGS_THEIRS) {
            $merged->flags = clone $translation->flags;
        } elseif (!($strategy & Catalog::FLAGS_OURS)) {
            $merged->flags = $merged->flags->mergeWith($translation->flags);
        }

        $override = (bool) ($strategy & Catalog::TRANSLATIONS_OVERRIDE);

        if (!$merged->translation || ($translation->translation && $override)) {
            $merged->translation = $translation->translation;
        }

        if (!$merged->plural || ($translation->plural && $override)) {
            $merged->plural = $translation->plural;
        }

        if (empty($merged->pluralTranslations) || (!empty($translation->pluralTranslations) && $override)) {
            $merged->pluralTranslations = $translation->pluralTranslations;
        }

        $merged->disable($translation->isDisabled());

        return $merged;
    }

    public function isEmpty(): bool
    {
        if ( !empty($this->getOriginal()) ) {
            return false;
        }

        if ( !$this->isTranslated() ) {
            return false;
        }

        return true;
    }

    public function isTranslated(): bool
    {
        return isset($this->translation) && $this->translation !== '';
    }

    public function withContext( ?string $context ): TranslationInterface
    {
        $clone          = clone $this;
        $clone->context = $context;
        $clone->id      = static::generateId($clone->getContext(), $clone->getOriginal());

        return $clone;
    }

    public function withOriginal( string $original ): TranslationInterface
    {
        $clone           = clone $this;
        $clone->original = $original;
        $clone->id       = static::generateId($clone->getContext(), $clone->getOriginal());

        return $clone;
    }

}

