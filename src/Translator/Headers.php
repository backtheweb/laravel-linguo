<?php

namespace Backtheweb\Linguo\Translator;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;

use Backtheweb\Linguo\Contracts\HeadersInterface;

use InvalidArgumentException;

class Headers implements HeadersInterface, JsonSerializable, Countable, IteratorAggregate
{
    const HEADER_DOMAIN         = 'X-Domain';
    const HEADER_LANGUAGE       = 'Language';
    const HEADER_PLURAL_FORM    = 'Plural-Forms';

    protected $headers = [
        'Project-Id-Version'            => null,
        'Report-Msgid-Bugs-To'          => null,
        'Last-Translator'               => null,
        'Language-Team'                 => null,
        'MIME-Version'                  => 1.0,
        'Content-Type'                  => 'text/plain; charset=UTF-8',
        'Content-Transfer-Encoding'     => '8bit',
        'POT-Creation-Date'             => null,
        'PO-Revision-Date'              => null,
        self::HEADER_LANGUAGE           => null,
        self::HEADER_DOMAIN             => 'default',
        self::HEADER_PLURAL_FORM        => 'nplurals=2; plural=n != 1;',
        'X-Generator'                   => ''
    ];

    public static function __set_state(array $state): Headers
    {
        return new static($state['headers']);
    }

    public function __construct(array $headers = [])
    {
        $this->headers = $headers;
        ksort($this->headers);
    }

    public function count()
    {
        return count($this->headers);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->toArray());
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return $this->headers;
    }

    public function set(string $name, string $value): HeadersInterface
    {
        $this->headers[$name] = trim($value);
        ksort($this->headers);

        return $this;
    }

    public function get(string $name): ?string
    {
        return $this->headers[$name] ?? null;
    }

    public function delete(string $name): HeadersInterface
    {
        unset($this->headers[$name]);

        return $this;
    }

    public function clear(): self
    {
        $this->headers = [];

        return $this;
    }

    public function setLocale($locale): self
    {
        return $this->set(self::HEADER_LANGUAGE, $locale);
    }

    public function getLocale()
    {
        return $this->get(self::HEADER_LANGUAGE);
    }

    public function setDomain(string $domain): self
    {
        return $this->set(self::HEADER_DOMAIN, $domain);
    }

    public function getDomain(): ?string
    {
        return $this->get(self::HEADER_DOMAIN);
    }

    public function setPluralForm($pluralForm): self
    {
        return $this->set(self::HEADER_PLURAL_FORM, $pluralForm);
    }

    public function getPluralForm(): ?string
    {
        return $this->get(self::HEADER_PLURAL_FORM);
    }

    public function mergeWith(Headers $headers): Headers
    {
        $merged = clone $this;
        $merged->headers = $headers->headers + $merged->headers;
        ksort($merged->headers);

        return $merged;
    }
}
