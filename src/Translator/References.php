<?php

namespace Backtheweb\Linguo\Translator;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;

use Backtheweb\Linguo\Contracts\ReferencesInterface;

class References implements ReferencesInterface, JsonSerializable, Countable, IteratorAggregate
{
    protected $references = [];

    public static function __set_state(array $state): References
    {
        $references = new static();
        $references->references = $state['references'];

        return $references;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->references);
    }

    public function count()
    {
        return array_reduce($this->references, function ($carry, $item) {
            return $carry + (count($item) ?: 1);
        }, 0);
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return $this->references;
    }

    public function add(string $filename, int $line = null): ReferencesInterface
    {
        $fileReferences = $this->references[$filename] ?? [];

        if (isset($line) && !in_array($line, $fileReferences)) {
            $fileReferences[] = $line;
        }

        $this->references[$filename] = $fileReferences;

        return $this;
    }

    public function mergeWith(References $references): self
    {
        $merged = clone $this;

        foreach ($references as $filename => $lines) {
            if (empty($lines)) {
                $merged->add($filename);
                continue;
            }

            foreach ($lines as $line) {
                $merged->add($filename, $line);
            }
        }

        return $merged;
    }
}
