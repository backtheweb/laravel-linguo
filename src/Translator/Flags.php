<?php

namespace Backtheweb\Linguo\Translator;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;

use Backtheweb\Linguo\Contracts\FlagsInterface;

class Flags implements FlagsInterface, JsonSerializable, Countable, IteratorAggregate
{
    protected $flags = [];

    public static function __set_state(array $state): Flags
    {
        return new static(...$state['flags']);
    }

    public function __construct(string ...$flags)
    {
        if (!empty($flags)) {
            $this->add(...$flags);
        }
    }

    public function getIterator()
    {
        return new ArrayIterator($this->flags);
    }

    public function count()
    {
        return count($this->flags);
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return $this->flags;
    }

    public function add(string ...$flags): FlagsInterface
    {
        foreach ($flags as $flag) {
            if (!$this->has($flag)) {
                $this->flags[] = $flag;
            }
        }

        sort($this->flags);

        return $this;
    }

    public function has(string $flag): bool
    {
        return in_array($flag, $this->flags, true);
    }

    public function mergeWith(Flags $flags): self
    {
        $merged = clone $this;
        $merged->add(...$flags->flags);

        return $merged;
    }

}
