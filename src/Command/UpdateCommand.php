<?php
namespace Backtheweb\Linguo\Command;

use Illuminate\Console\Command;
use League\Flysystem\Exception;

use Backtheweb\Linguo\Engines\Po;
use Backtheweb\Linguo\Translator\Catalog;
use Backtheweb\Linguo\Translator\Translation;

class UpdateCommand extends Command
{
    protected $signature      = 'linguo:update';

    protected $description = 'Update po files from pot file';

    /**
     * @return int
     * @throws Exception
     */
    public function handle()
    {
        $config       = (object) config('linguo');
        $pot_file     = "{$config->target}/catalog.pot";

        if(!is_file($pot_file)){

            $this->line(sprintf('<error>POT file not found</error> Run: <info>php artisan linguo:scan</info>', $pot_file));

            return null;
        }

        $pot = ( new Po() )->loadFile($pot_file);

        // $config->locales = ['en_US'];

        foreach ($config->locales as $locale){

            $po_file = "{$config->target}/{$locale}/default.po";

            if( !is_file($po_file) ){

                $folder = dirname($po_file);

                if(!is_dir($folder)){
                    mkdir($folder);
                }

                copy($pot_file, $po_file);
            }

            $catalog_po = ( new Po() )->loadFile($po_file);


            $before = $catalog_po->count();
            $merge  = $catalog_po->mergeWith($pot,
                Catalog::HEADERS_THEIRS |
                        Catalog::TRANSLATIONS_THEIRS |
                        Catalog::TRANSLATIONS_OVERRIDE |
                        Catalog::EXTRACTED_COMMENTS_OURS |
                        Catalog::REFERENCES_OURS |
                        Catalog::FLAGS_THEIRS |
                        Catalog::COMMENTS_THEIRS
            );
            $after = $merge->count();

            $this->line(sprintf('File: <info>%s</info> :: %d => %d', $po_file, $before, $after));

            ( new Po() )->generateFile( $merge, $po_file);
        }
    }
}
