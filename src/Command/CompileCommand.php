<?php
namespace Backtheweb\Linguo\Command;

use Backtheweb\Linguo\Engines\JsonLaravel;
use Backtheweb\Linguo\Engines\Po;
use Illuminate\Console\Command;

class CompileCommand extends Command
{
    protected $signature = 'linguo:compile {--merge} ';

    protected $description = 'Compile po files to php';

    /**
     * @throws \Exception
     */
    public function handle()
    {

        $merge = $this->option('merge');

        $config = (object) config('linguo');

        foreach ($config->locales as $locale){

            $source_file = "{$config->target}/{$locale}/default.po";
            $target_file = "{$config->target}/{$locale}.json";

            if(!is_file($source_file)){

                $this->line(sprintf('<error>Source file not found</error> %s', $source_file));

                continue;
            }

            $this->line(sprintf('<info>%s</info> :: %s', $source_file, $target_file ));

            $translations = (new Po())->loadFile($source_file);

            (new JsonLaravel())->generateFile( $translations, $target_file, $merge);
        }
    }
}
