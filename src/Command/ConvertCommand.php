<?php
namespace Backtheweb\Linguo\Command;

use Backtheweb\Linguo\Engines\JsonLaravel;
use Backtheweb\Linguo\Engines\PhpArray;
use Illuminate\Console\Command;
use League\Flysystem\Exception;

use Backtheweb\Linguo\Engines\Po;
use Backtheweb\Linguo\Translator\Catalog;
use Backtheweb\Linguo\Translator\Translation;

class ConvertCommand extends Command
{
    protected $signature = 'linguo:convert {--format=array} {file}';

    /**
     * @return |null
     * @throws \Exception
     */
    public function handle()
    {
        $config   = (object) config('linguo');

        $file     = $this->argument('file');
        $format   = $this->option('format');
        $source   = resource_path( $file );

        if(!is_file($source)){

            $this->line(sprintf('<error>File not found</error>', $source));

            return null;
        }

        $info = pathinfo($source);

        switch ($info['extension']){

            case 'pot':
            case 'po':      $loader = new Po;          break;
            case 'json' :   $loader = new JsonLaravel; break;
            case 'php' :    $loader = new PhpArray();  break;
        }

        switch($format) {

            default:
            case 'array':

                $generator = new PhpArray;
                $target    = sprintf('%s/%s.%s', $info['dirname'], $info['filename'], 'php' );
        }



        $catalog = $loader->loadFile( $source );
        $saved   = $generator->generateFile( $catalog, $target );


        $this->line(sprintf('%s %s', $saved ? '<info>[Ok]</info>' : '<error>[Error]</error>', $target));
    }
}
