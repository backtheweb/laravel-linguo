<?php
namespace Backtheweb\Linguo\Command;

use Backtheweb\Linguo\Scanner\BladeScanner;
use Backtheweb\Linguo\Scanner\TwigScanner;
use Backtheweb\Linguo\Scanner\PhpScanner;
use Illuminate\Console\Command;
use Backtheweb\Linguo\Engines\Po;
use Backtheweb\Linguo\Translator\Catalog;

class ScanCommand extends Command
{
    protected $signature  = 'linguo:scan {domain?}';

    protected $description = 'Scan files and create or update pot file';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $config   = (object) config('linguo');
        $reader   = function($root) use (&$reader) {

            $paths    = [];
            $files    = [];
            $iterator = new \RecursiveIteratorIterator(
                        new \RecursiveDirectoryIterator($root, \RecursiveDirectoryIterator::SKIP_DOTS),
                     \RecursiveIteratorIterator::SELF_FIRST,
                      \RecursiveIteratorIterator::CATCH_GET_CHILD
            );

            foreach ($iterator as $path => $file) {

                if ( $file->isDir() ) {

                    $f     = $reader($file);
                    $files = array_merge($files, $f);

                } else {

                    $paths[] = (string) $file;
                }
            }

            return $paths;
        };

        $sources  = [];
        $domain   = $this->argument('domain') ?? $config->domain;
        $domains  = $config->domains ?? [];
        $target   = $config->target;
        $pot_file = "{$target}/catalog.pot";

        foreach($config->paths as $path) {

            $found    = $reader($path);
            $sources  = array_merge($sources, $found);
        }

        $catalog = Catalog::factory($domain);

        foreach($sources as $file){

            $collection = null;

            switch(true){

                case str_ends_with($file, '.twig')      : $collection = TwigScanner::scan($file); break;
                case str_ends_with($file, '.blade.php') : $collection = BladeScanner::scan($file);  break;
                case str_ends_with($file, '.php')       : $collection = PhpScanner::scan($file);  break;

                default: $this->error ('missing scanner for ' . $file);
            }

            if($collection){

                $collection->each(function ($item) use ($catalog, $domain, $domains){

                    if( count( $domains ) && preg_match("/^".implode("|", $domains )."/", $item->getOriginal())) {

                    } else {

                        $catalog->add($item);
                    }
                });
            }
        }

        ( new Po() )->generateFile($catalog, $pot_file);

        $catalog_x = ( new Po() )->loadFile($pot_file);

        $this->line( sprintf('<info>%d</info> %d translations found', $catalog_x->count(), count($catalog_x->getTranslations() ) ));
    }


}
