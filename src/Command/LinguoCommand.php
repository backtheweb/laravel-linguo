<?php
namespace Backtheweb\Linguo\Command;

use Illuminate\Console\Command;

class LinguoCommand extends Command
{
    /**
     * @var string
     */
    protected $signature      = 'linguo';

    /**
     * @var string
     */
    protected $description = 'Scan files and update po files';

    /**
     * @return int
     * @throws Exception
     */
    public function handle()
    {
        $this->info('Scan files from paths');

        foreach (config('linguo.paths') as $path){
            $this->line("\t" . $path);
        }

        $this->call('linguo:scan');
        $this->info('Update po files');
        $this->call('linguo:update');
    }
}
