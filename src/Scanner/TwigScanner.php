<?php

namespace Backtheweb\Linguo\Scanner;

use Backtheweb\Linguo\Translator\Translation;
use Illuminate\Support\Collection;

class TwigScanner extends Scanner
{

    /**
     * @param $file
     * @return Collection
     */
    public static function scan($file) : Collection
    {

        $collection = Collection::make([]);

        $subject = file_get_contents($file);
        $subject = preg_replace('/{#(.*?)#}/', '', $subject);

        // SINGULAR

        $pattern = "/__\(['\"](.*?)['\"]\D*?\)?/";
        $matches = [];

        preg_match_all($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);

        $count   = count($matches[0]);

        //$this->line( sprintf("<info>%s</info>", $file) );

        if($count){

            //$this->line( "\t" . sprintf("<comment>Singulars</comment>") );

            foreach($matches[1] as $m) {

                $original = $m[0];
                $pos      = $m[1];
                $plural   = null;

                list($before) = str_split($subject, $pos );
                $line         = strlen($before) - strlen(str_replace("\n", "", $before)) + 1;
                $tr           = Translation::factory( $original, $plural, $file, $line, null );

                $collection->add($tr);

                //$this->line( sprintf("\t\t<info>%s</info>: %s", $line, $original) );
            }
        }

        // PLURAL

        $pattern = "/__n\(['\"](.*?)['\"],\D*?['\"](.*?)['\"],?\D*?/";
        $matches = [];

        preg_match_all($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);

        $count = count($matches[0]);

        if($count){

            //$this->line( "\t" . sprintf("<comment>Plurals</comment>") );

            $singulars = $matches[1];
            $plurals   = $matches[2];
            $count     = count($singulars);

            for($i = 0, $max = $count; $i < $max; $i++) {

                $original = $singulars[$i][0];
                $plural   = $plurals[$i][0];
                $pos      = $singulars[$i][1];

                list($before) = str_split($subject, $pos );
                $line         = strlen($before) - strlen(str_replace("\n", "", $before)) + 1;
                $tr           = Translation::factory( $original, $plural, $file, $line, null );

                $collection->add($tr);

                //$this->line( sprintf("\t\t%s|%s:<info>%s</info>", $original, $plural, $line) );

            }

            //$this->line( "\t" . sprintf("Found: <info>%s</info>\n", $found) );
        }

        return $collection;
    }
}
