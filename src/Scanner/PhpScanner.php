<?php

namespace Backtheweb\Linguo\Scanner;

use Backtheweb\Linguo\Translator\Translation;
use Illuminate\Support\Collection;

class PhpScanner extends Scanner
{

    /**
     * @param $file
     * @return Collection
     */
    public static function scan($file) : Collection
    {

        $collection = Collection::make([]);
        $subject    = file_get_contents($file);

        // SINGULAR

        $pattern = "/__\(['\"](.*?)['\"]\D*?\)?/";
        $matches = [];

        preg_match_all($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);

        $count   = count($matches[0]);

        if($count){

            foreach($matches[1] as $m) {

                $original = $m[0];
                $pos      = $m[1];
                $plural   = null;

                list($before) = str_split($subject, $pos );
                $line         = strlen($before) - strlen(str_replace("\n", "", $before)) + 1;
                $tr           = Translation::factory( $original, $plural, $file, $line, null );

                $collection->add($tr);
            }
        }

        // PLURAL

        $pattern = "/__n\(['\"](.*?)['\"],\D*?['\"](.*?)['\"],?\D*?/";
        $matches = [];

        preg_match_all($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);

        $count = count($matches[0]);

        if($count){

            $singulars = $matches[1];
            $plurals   = $matches[2];
            $count     = count($singulars);

            for($i = 0, $max = $count; $i < $max; $i++) {

                $original = $singulars[$i][0];
                $plural   = $plurals[$i][0];
                $pos      = $singulars[$i][1];

                list($before) = str_split($subject, $pos );
                $line         = strlen($before) - strlen(str_replace("\n", "", $before)) + 1;
                $tr           = Translation::factory( $original, $plural, $file, $line, null );

                $collection->add($tr);
            }
        }

        return $collection;
    }
}
