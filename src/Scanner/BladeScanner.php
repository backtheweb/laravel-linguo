<?php

namespace Backtheweb\Linguo\Scanner;


use Backtheweb\Linguo\Translator\Translation;
use Illuminate\Support\Collection;

class BladeScanner extends Scanner
{
    /**
     * @param $file
     * @return Collection
     */
    public static function scan($file) : Collection
    {

        $collection = Collection::make([]);

        $subject = file_get_contents($file);
        $subject = preg_replace('/<!--(.*?)-->/', '', $subject);


        $pattern = "/__\(['\"](.*?)['\"]\D*?\)?/";
        $matches = [];

        preg_match_all($pattern, $subject, $matches, PREG_OFFSET_CAPTURE);

        $count   = count($matches[0]);


        if($count){

            foreach($matches[1] as $m) {

                $original = $m[0];
                $pos      = $m[1];
                $plural   = null;

                list($before) = str_split($subject, $pos );
                $line         = strlen($before) - strlen(str_replace("\n", "", $before)) + 1;
                $tr           = Translation::factory( $original, $plural, $file, $line, null );

                $collection->add($tr);

                //$this->line( sprintf("\t\t<info>%s</info>: %s", $line, $original) );
            }
        }

        return $collection;
    }
}
