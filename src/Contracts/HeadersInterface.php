<?php

namespace Backtheweb\Linguo\Contracts;

interface HeadersInterface
{

    public function set(string $name, string $value): self;

    public function get(string $name): ?string;

    public function delete(string $name): self;

    public function getPluralForm(): ?string;
}


