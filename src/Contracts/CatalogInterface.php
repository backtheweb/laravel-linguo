<?php

namespace Backtheweb\Linguo\Contracts;

use phpDocumentor\Reflection\Types\Integer;

interface CatalogInterface
{
    public function toArray() : array;

    public function getTranslations() : array;

    public function count() : int;
}
