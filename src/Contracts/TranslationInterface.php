<?php

namespace Backtheweb\Linguo\Contracts;


use Gettext\Translation;

interface TranslationInterface
{

    public function toArray() : array;

    public function getOriginal(): ?string;

    public function getTranslation(): ?string;

    public function isTranslated(): Bool;

    public function isEmpty(): bool;

    public static function factory( string $original, string $plural = null, string $file = null, $line = null, string $context = null ) : TranslationInterface;

    public function disable( bool $disabled = true ) : self;

    public function getReferences() : ReferencesInterface;

    public function getFlags(): FlagsInterface;

    public function getComments() : CommentsInterface;

    public function getExtractedComments() : CommentsInterface;

    public function withContext( ?string $context ) : self;

    public function withOriginal( string $original ) : self;

    public function hasPluralTranslations() : bool;

    public function getPluralTranslations(int $size = null) : array;

}
