<?php

namespace Backtheweb\Linguo\Contracts;

interface LoaderInterface
{
    public function loadFile(string $filename, CatalogInterface $translations = null): CatalogInterface;

    public function loadString(string $string, CatalogInterface $translations = null): CatalogInterface;
}
