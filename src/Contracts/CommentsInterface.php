<?php

namespace Backtheweb\Linguo\Contracts;

interface CommentsInterface
{

    public function toArray() : array;

    public function add(string ...$comments): self;
}
