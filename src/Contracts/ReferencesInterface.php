<?php

namespace Backtheweb\Linguo\Contracts;

interface ReferencesInterface
{

    public function toArray() : array;

    public function add(string $filename, int $line = null): self;
}
