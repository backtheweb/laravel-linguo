<?php

namespace Backtheweb\Linguo\Contracts;

interface FlagsInterface
{
    public function toArray() : array;

    public function add(string ...$flags): self;
}
