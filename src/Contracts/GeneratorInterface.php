<?php

namespace Backtheweb\Linguo\Contracts;

interface GeneratorInterface
{
    public function generateFile(CatalogInterface $catalog, string $filename): bool;

    public function generateString(CatalogInterface $catalog): string;
}
