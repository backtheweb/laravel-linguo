# linguo
Laravel i18n parser

## Installation

### Laravel Service Provider

Add the service provider and the face on `config/app.php`

```
'providers' => [

    [...]
    Backtheweb\Linguo\LinguoServiceProvider::class,
],

'aliases' => [
    [...]
    'Linguo' => Backtheweb\Linguo\LinguoFacade::class,
]
```   
 
And publish the config:

```
php artisan vendor:publish --provider="Backtheweb\Linguo\LinguoServiceProvider" --tag=config
```

### Setup

Edit `config/linguo.php`

````
return [

    'headers' => [
        'Project-Id-Version'    => env('APP_NAME', 'linguo'),
        'Language-Team'         => 'TeamName <info@example.com>'
    ],

    'paths' => [
        base_path('resources/views'),
        base_path('app/Http/Controllers'),
    ] ,

    'target'    =>  resource_path('lang'),
    'domain'    => 'default',
    'domains'   => [
        'auth',
        'pagination',
        'passwords',
        'validation',
    ],      // ignore domains, filenames on langs folder such as pagination, passwords,...

    'locales'   => ['es_ES', 'en_US', 'it_IT', 'fr_FR', 'de_DE', 'pt_BR'],
];
````

**Sources** : Is where the parser will be looking for translation keys
**i18nPath**: Is the path where are stored the translation files 

## Parse and generate translation files

### Parse

    php artisian linguo:scan        // Create file
    php artisian linguo:update      // Create or update po files
    php artisian linguo:compile     // Convert po to json

