<?php

return [

    'headers' => [
        'Project-Id-Version'    => env('APP_NAME', 'linguo'),
        'Language-Team'         => 'TeamName <info@example.com>'
    ],

    'paths' => [
        base_path('resources/views'),
        base_path('app/Http/Controllers'),
    ] ,

    'target'    =>  lang_path(),
    'domain'    => 'default',
    'domains'   => [
        'auth',
        'pagination',
        'passwords',
        'validation',
    ],

    'locales'   => ['ca', 'es', 'en'],
];
